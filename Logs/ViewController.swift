//
//  ViewController.swift
//  Logs
//
//  Created by Ricardo Santos on 16/07/2020.
//  Copyright © 2020 Ricardo Santos. All rights reserved.
//

import UIKit
import Foundation

func getBodyFrom(parts: [String], date: String) -> String {
    var index = 0
    var body: String = ""
    parts.forEach { (some) in
        var ignoreLine = false
        ignoreLine = ignoreLine || some.count == 0
        ignoreLine = ignoreLine || some.contains("\(date)")
        //ignoreLine = ignoreLine || some.contains("[QA release]") || some.contains("[Prod release]")
        ignoreLine = ignoreLine || (some.contains("Memory") && some.contains("MB"))
        ignoreLine = ignoreLine || some.contains("<<<<<<<<<<<<<<<<<<")
        ignoreLine = ignoreLine || some.contains(">>>>>>>>>>>>>>>>>>")
        ignoreLine = ignoreLine || some.contains("Front-most view controller")
        index += 1
        var someCopy = some
        while someCopy.hasSuffix("\n") || someCopy.hasSuffix(",") {
            someCopy = "\(someCopy.dropLast())"
        }
        if !ignoreLine {
            body = "\(body)\(someCopy)\n"
        }
    }
    return body
}

func analiseSpecificAPIService(someLogToAnalise: Logs, serviceName: String, filterByDelayBiggerThan: Float? = nil) {
    var records: [String] = []
    var logToAnalise = someLogToAnalise
    if serviceName.count > 0 {
        records = logToAnalise.logs.recordsWith(content: serviceName)
    } else {
        records = logToAnalise.servicesResponses
    }
    var recordsMatching: [String] = records
    if let filterByDelayBiggerThan = filterByDelayBiggerThan {
        recordsMatching = records.withDelayBigger(than: filterByDelayBiggerThan)
    }
    if serviceName.count > 0 {
        print("\(records.count) calls to API [\(serviceName)]")
    }
    if let filterByDelayBiggerThan = filterByDelayBiggerThan, filterByDelayBiggerThan > 0 {
        print(" - Records with delay bigger than \(filterByDelayBiggerThan)s : \(recordsMatching.count)")
    }
    let min: Float = records.minDelay
    let max: Float = records.maxDelay
    let med: Float = records.mediumDelay
    print(" Responses Delay :  med=\(med)s, min=\(min)s, max=\(max)s")
}

func fullAnaliseFor(log: Logs,
                    prefix: String,
                    showSlowRequests: Bool,
                    showScreenFlow: Bool,
                    showErrors: Bool,
                    showAPICalls: Bool,
                    showAppDelegateLifeCycle: Bool,
                    showSilentPushs: Bool,
                    printStats: Bool,
                    servicesToDebug: [String]) {

    
    var logsCopy: Logs = log

    print("")
    print("#################################################")
    print("# \(prefix)")
    print("#  . showSlowRequests = \(showSlowRequests)")
    print("#  . showScreenFlow = \(showScreenFlow)")
    print("#  . servicesToDebug = \(servicesToDebug)")
    print("#  . showErrors = \(showErrors)")
    print("#  . showSilentPushs = \(showSilentPushs)")
    print("#  . showAPICalls = \(showAPICalls)")
    print("#  . showAppDelegateLifeCycle = \(showAppDelegateLifeCycle)")
    print("#  . printStats = \(printStats)")
    print("#################################################")
    guard logsCopy.logs.count > 0 else {
        print("No logs to parse")
        return
    }
    if printStats {
        if var first = logsCopy.logs.first {
            if first.contains("Session UUID,App version,App build,Language,OS version") {
                first = logsCopy.logs[1]
            }
            let parts = first.components(separatedBy: "\r")
            let date = parts[0].extrateDate
            print("# First Log       : \(date!)")
        }

        if let last = logsCopy.logs.last {
            let parts = last.components(separatedBy: "\r")
            let date = parts[0].extrateDate
            print("# Last Log        : \(date!)")
        }
        print("# Total Logs      : \(logsCopy.logs.count)")
        print("# Total Errors    : \(logsCopy.errors.count)")
        print("# Total API calls : \(logsCopy.servicesRequests.count)")
        let servicesNotOkCount: Float = Float(logsCopy.servicesNotOK.count)
        let min: Float = logsCopy.servicesResponses.minDelay
        let max: Float = logsCopy.servicesResponses.maxDelay
        let med: Float = logsCopy.servicesResponses.mediumDelay
        print("#  - Failed  : \(servicesNotOkCount)x | \((servicesNotOkCount/Float(logsCopy.servicesRequests.count))*100)%")
        print("#  - Responses Delay : med=\(med)s, min=\(min)s, max=\(max)s")

        let servicesSlow_1: Float = Float(logsCopy.servicesResponses.withDelayBigger(than: 1).count)
        if servicesSlow_1 > 0 {
            print("#  - Responses > 1s  : \(Int(servicesSlow_1))x | \((servicesSlow_1/Float(logsCopy.servicesRequests.count))*100)%")
        }

        let servicesSlow_3: Float = Float(logsCopy.servicesResponses.withDelayBigger(than: 3).count)
        if servicesSlow_3 > 0 {
            print("#  - Responses > 3s  : \(Int(servicesSlow_3))x | \((servicesSlow_3/Float(logsCopy.servicesRequests.count))*100)%")
        }

        let servicesSlow_5: Float = Float(logsCopy.servicesResponses.withDelayBigger(than: 5).count)
        if servicesSlow_5 > 0 {
            print("#  - Responses > 5s  : \(Int(servicesSlow_5))x | \((servicesSlow_5/Float(logsCopy.servicesRequests.count))*100)%")
        }

        let servicesSlow_10: Float = Float(logsCopy.servicesResponses.withDelayBigger(than: 10).count)
        if servicesSlow_10 > 0 {
            print("#  - Responses > 10s : \(Int(servicesSlow_10))x | \((servicesSlow_10/Float(logsCopy.servicesRequests.count))*100)%")
        }

        let servicesSlow_20: Float = Float(logsCopy.servicesResponses.withDelayBigger(than: 20).count)
        if servicesSlow_20 > 0 {
            print("#  - Responses > 20s : \(Int(servicesSlow_20))x | \((servicesSlow_20/Float(logsCopy.servicesRequests.count))*100)%")
        }
        print("#################################################\n\n")
    }


    var index = 0
    logsCopy.logs.forEach { (some) in
        let parts = some.components(separatedBy: "\r")
        let date = parts[0].extrateDate
        var body = ""
        var bodyExtraDebug = ""
        parts.forEach { (some) in

            var fullDebugThis = false

            _ = "🟧🟥🟨🟩🟦🟪🟫🔴🟠🟡🟢🔵🟣⚪"

            //
            // App Delegate
            //

            if showAppDelegateLifeCycle {
                let isAppLifeCycleDid = some.lowercased().contains("application.") && some.lowercased().contains("did")
                let isAppLifeCycleWill = some.lowercased().contains("application.") && some.lowercased().contains("will")
                if isAppLifeCycleDid || isAppLifeCycleWill {
                    var someParsed = some.replacingOccurrences(of: "[Prod release] ", with: "")
                    someParsed = some.replacingOccurrences(of: "[QA release] ", with: "")
                    if some.contains("application.didFinishLaunchingWithOptions:") {
                        body = ""
                        print("\n############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################")
                        print("#")
                        let dateParsed = "\(date!)".replacingOccurrences(of: "# ", with: "")
                        print("# 🟦 AppDelegate:[\(someParsed)]\n# 🟦 \(dateParsed) ")
                        print("#")
                        print("############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################")
                        print("############################# APP STARTED #############################\n")
                    } else {
                        body = "🟦 AppDelegate:[\(someParsed)]"
                    }
                }
            }

            //
            // Screens Flow
            //

            let isViewLifeCicle1 = some.contains("VCLifeCycle") && !some.contains("logVCLifeCycle")
            let isViewLifeCicle2 = some.contains("Will switch to")
            let isViewLifeCicle3 = some.contains("Will present")
            if showScreenFlow && (isViewLifeCicle1 || isViewLifeCicle2 || isViewLifeCicle3) && body.count == 0 {
                var ignore = false
                ignore = ignore || some.contains("@ doViewLifeCycle")
                ignore = ignore || some.contains("@ deinit")
                if !ignore {
                    var escaped = some
                    escaped = escaped.replacingOccurrences(of: "# VCLifeCycle", with: "VCLifeCycle")
                    escaped = escaped.replacingOccurrences(of: "[Prod release] VCLifeCycle", with: "VCLifeCycle")
                    escaped = escaped.replacingOccurrences(of: "[Prod release] Will switch to", with: "Will switch to")
                    body = "🟩 \(escaped)"
                } else {
                    body = ""
                }
            }

            //
            // Silent Push
            //

            if showSilentPushs && some.contains("Received Silent Push") && body.count == 0 {
                parts.forEach { (some) in
                    var ignore = false
                    ignore = ignore || some.contains("\(date!)")
                    ignore = ignore || some.contains(">>>>>>>>>>>>>>")
                    ignore = ignore || some.contains("<<<<<<<<<<<<<<")
                    ignore = ignore || some.contains("# Memory:")
                    ignore = ignore || some.contains("didReceiveSilentPush")
                    ignore = ignore || some.contains("[RxCocoa.ControlTarget eventHandler:]")
                    ignore = ignore || some.contains("Front-most view controller")
                    ignore = ignore || some.contains("{✅✅✅}<")
                    if !ignore {
                        body = "\(body)\(some)\n"
                    }
                }

                body = body.replacingOccurrences(of: "[Prod release] ", with: "")
                body = body.replacingOccurrences(of: "[QA release] ", with: "")
                body = body.replacingOccurrences(of: "Received Silent Push: ", with: "")
                body = body.replacingOccurrences(of: "\"\"", with: "\"")
                body = body.replacingOccurrences(of: "=     (", with: "=")
                body = body.replacingOccurrences(of: "    );", with: ";")
                body = body.replacingOccurrences(of: "        ", with: " ")
                body = body.replacingOccurrences(of: "    ", with: " ")
                body = body.replacingOccurrences(of: "\n", with: "")
                body = "🟦 Silent Push\n\n\(body)"
            }


            //
            // API Services
            //

            if showAPICalls && (some.contains("⤴️") || some.contains("⤵️")) && body.count == 0 {
                let isResponse = some.contains("⤵️")
                let isRequest = some.contains("⤴️")
                if isResponse {
                    print(isResponse)
                }
                parts.forEach { (some) in

                    servicesToDebug.forEach { (someServiceNameToDebug) in
                        if some.contains(someServiceNameToDebug) && some.contains("https:") {
                            fullDebugThis = true && isResponse
                        }
                    }

                    //if fullDebugThis {
                    //    print(some)
                    //}
                    var addLineToBodyLog = false
                    addLineToBodyLog = addLineToBodyLog || some.contains("https://")
                    addLineToBodyLog = addLineToBodyLog || some.contains("Request Delay")
                    addLineToBodyLog = addLineToBodyLog || some.contains("StatusCode")
                    let isBody = (some.contains("meta") && some.contains("request-id") && some.contains("links"))
                    if isBody {
                        addLineToBodyLog = fullDebugThis ? true : false
                    }
                    if addLineToBodyLog {
                        body = "\(body)\(some)\n"
                    }
                }
                body = body.replacingOccurrences(of: "\"\"", with: "\"")
                body = body.replacingOccurrences(of: "- Request : ", with: "")
                body = body.replacingOccurrences(of: "- Request   : ", with: "")
                body = body.replacingOccurrences(of: "- Request      : ", with: "")
                if isRequest {
                    body = "⤴️\(body)"
                } else {
                    body = "⤵️\(body)"
                }
                if body.hasSuffix("\n") {
                    body = "\(body.dropLast())"
                }

                if fullDebugThis {
                    bodyExtraDebug = "\(bodyExtraDebug)\n\(some)"
                }
            }
        }

        //
        // Errors
        //

        if showErrors && some.contains("⛔️") && body.count == 0 {
            body = getBodyFrom(parts: parts, date: "\(date!)")

            if body.contains("localized not found from") {
                body = "\(body.split(separator: "\n").first!)"
            }

            body = ""

            var ignoreLog = false
            ignoreLog = ignoreLog || some.contains("localized not found from []")        // Falso positivo
            ignoreLog = ignoreLog || some.contains("localized not found for []")         // Falso positivo
            ignoreLog = ignoreLog || some.contains("welcome.error.invalid_phone_number") // Falso positivo
            ignoreLog = ignoreLog || some.contains("authentication_prompt")              // Falso positivo

            ignoreLog = ignoreLog || some.contains("Alamofire.AFError.explicitlyCancelled") // Happens a lot
            ignoreLog = ignoreLog || some.contains("symmetricKeyInvalid")                   // Happens a lot
            ignoreLog = ignoreLog || some.contains("noCredentialsForAutoLogin")             // Happens a lot

            if !ignoreLog {
                parts.forEach { (some) in
                    //print(some)
                    var ignore = false
                    ignore = ignore || some.contains(">(main) #")
                    ignore = ignore || some.contains(">(ModuleProfile) #")
                    ignore = ignore || some.contains("<<<<<<<<<<<<<<")
                    ignore = ignore || some.contains(">>>>>>>>>>>>>>")
                    ignore = ignore || some.contains("challengeAwareRequest")
                    ignore = ignore || some.contains("String+Extensions")
                    ignore = ignore || some.contains("Set device data")
                    ignore = ignore || some.contains("[RxCocoa.ControlTarget eventHandler:]")
                    ignore = ignore || some.contains("Front-most view controller")
                    ignore = ignore || some.contains("com.apple")
                    ignore = ignore || some.contains("PrimitiveSequence+Extensions")
                    ignore = ignore || some.contains(",AppErrorLogger,")
                    ignore = ignore || some.contains("W.SubscriptionWorker+Subscription")
                    if !ignore {
                        var escaped = some.replacingOccurrences(of: "\"\"", with: "\"")
                        escaped = escaped.replacingOccurrences(of: "[Prod release] ", with: "")
                        escaped = escaped.replacingOccurrences(of: "[QA release] ", with: "")
                        while escaped.hasSuffix("\n") || escaped.hasSuffix(",") {
                            escaped = "\(escaped.dropLast())"
                        }
                        escaped = escaped.replacingOccurrences(of: ",,", with: ",")
                        if body.count == 0 {
                            body = "⛔️ : \(escaped)"
                        } else {
                            body = "\(body)\n\(escaped)"
                        }
                    }
                }
            }
        }

        if !body.isEmpty {
            print("-----------------------------------------------------------------")
            print("------------------- \(date!) -------------------\n\n\(body)\n")
        }

        index += 1

    }

    if showSlowRequests {
        let time: Float = 10
        print("\n--------------------------------------------------")
        print("- Slow Services stats (response delay > \(time)s")
        print("--------------------------------------------------")
        servicesToDebug.forEach { (service) in
            let records = logsCopy.logs.recordsWith(content: service).withDelayBigger(than: time)
            if records.count > 0 {
                print("\n### Found \(records.count) records for [\(service)] with delay > \(time)s\n")
                records.forEach { (some) in
                    print(some.serviceResponsePretty)
                }
            }
        }
    }

    if servicesToDebug.count > 0 {
        print("\n--------------------------------------------------")
        print("- Partial Services stats")
        print("- \(servicesToDebug)")
        print("--------------------------------------------------\n")

        servicesToDebug.forEach { (service) in
            analiseSpecificAPIService(someLogToAnalise: logsCopy, serviceName: service)
            print("")
        }
    }

    print("\n--------------------------------------------------")
    print("- Total Services stats")
    print("--------------------------------------------------\n")

    analiseSpecificAPIService(someLogToAnalise: logsCopy, serviceName: "")

}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //
        // servicesToDebug
        //
        // Possible value -> [], ["any service url part"]
        //
        var servicesToDebug = ["/main-branches", "/main-branches/districts", "/group-categories", "consents?tags=registration"]
        servicesToDebug = ["consents?tags=registration"]

        //
        // filterDate
        //
        // Possible values -> ["", "2020-07-21", "2020", "2020-07-21T00:00", ...]
        // If no filter date, will analyse all logs on csv file
        //
        var filterDate = "2020-07-21T08:5"
        filterDate = ""

        //
        // log file to analyse
        //
        var logsFile = ""

        ["d340875010_f2020-06-28T00_00_00+01_00"].forEach({ (file) in
            logsFile = file
            [filterDate].forEach { (day) in
                let someLogs = getLogs(file: logsFile, day: day)
                if day.count > 0 {
                    print("--------------------------------------------------------------------")
                    print("----------------------------> \(day) <--------------------------")
                }
                fullAnaliseFor(log: someLogs,
                               prefix: logsFile,
                               showSlowRequests: false,          // print slow services
                               showScreenFlow: true,            // Show user screens while navigating on app
                               showErrors: true,                // Show errors
                               showAPICalls: true,              // Show API requests/responses
                               showAppDelegateLifeCycle: true,  // Show application logs
                               showSilentPushs: true,           // Show silent push logs
                               printStats: true,                // Print generic API stats (
                               servicesToDebug: servicesToDebug // If not empty will print stats about service, and also print response body on API response
                )
            }
        })

    }
}
