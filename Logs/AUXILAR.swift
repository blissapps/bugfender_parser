//
//  File.swift
//  Logs
//
//  Created by Ricardo Santos on 16/07/2020.
//  Copyright © 2020 Ricardo Santos. All rights reserved.
//

import Foundation

public struct Logs {
    public var logs: [String] = []

    public lazy var errors: [String] = { logs.filter { $0.contains("⛔️") } }()
    public lazy var screens: [String] = { logs.filter { $0.contains("VCLifeCycle") } }()
    public lazy var services: [String] = { logs.filter { $0.contains("⤴️") || $0.contains("⤵️") } }()
    public lazy var servicesRequests: [String] = { logs.filter { $0.contains("⤴️") } }()
    public lazy var servicesResponses: [String] = { logs.filter { $0.contains("⤵️") } }()
    public lazy var servicesNotOK: [String] = {
        var result: [String] = []
        servicesResponses.forEach { (some) in
            let isOK_1 = some.contains(" - StatusCode   : 2") // all status from 200 -> 299
            let isOK_2 = some.contains("StatusCode   : N.A.")
            let allIsOK = isOK_1 || isOK_2
            if !allIsOK { result.append(some) }
        }
        return result
    }()
    public mutating func serviceResponseWith(statusCode: [Int]) -> [String] {
        return servicesResponses.filter { (some) -> Bool in
            var exists = false
            statusCode.forEach { (code) in if some.contains(" - StatusCode   : \(code)") { exists = true } }
            return exists
        }
    }
    public mutating func logsWith(content: String) -> [String] {
        return logs.filter { $0.contains(content) }
    }
}

public func getLogs(file: String, day: String?) -> Logs {
    guard let fileURL = Bundle.main.url(forResource: file, withExtension: "csv") else {
        print("Not found \(file)")
        return Logs()
    }
    let content = try? String(contentsOf: fileURL, encoding: String.Encoding.utf8)
    var logs: [String] = []
    var log = ""
    var recordDay = ""
    if let rows = content?.components(separatedBy: "\n") {
        rows.forEach { (someLine) in
            if day == nil || (day != nil && day!.isEmpty) {
                let isNewRecordStart = someLine.hasPrefix("# {") && someLine.contains(") #")
                if isNewRecordStart {
                    logs.append(log)
                    log = someLine
                } else {
                    log = "\(log)\(someLine)"
                }
            } else {
                // Filter by day
                let isNewRecordStart = someLine.hasPrefix("# {") && someLine.contains(") #")
                if isNewRecordStart {
                    recordDay = someLine
                    if recordDay.contains(day!) {
                        logs.append(log)
                        log = someLine
                    }
                } else {
                    if recordDay.contains(day!) {
                        log = "\(log)\(someLine)"
                    }
                }
            }
        }
    }
    return Logs(logs: logs)
}

extension Array where Element == Float {
    var mean: Double {
        let sum = self.reduce(0, { a, b in return a + b })
        let mean = Double(sum) / Double(self.count)
        return Double(mean)
    }
}

public extension String {
    var requestDelay: Float? {
        let lines = self.components(separatedBy: "\r")
        var result: Float?
        if self.contains("Time since boot:") {
            //  9,22 s
            lines.forEach { (line) in
                if line.contains("Time since boot:") {
                    var requestDelay = line.replacingOccurrences(of: " s", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "[Prod release] [DashboardLoadFinished] Timeince boot: ", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: ".", with: "", options: .literal, range: nil)
                    result = (requestDelay as NSString).floatValue
                }
            }
        } else {
            lines.forEach { (line) in
                if line.contains("Request Delay") {
                    var requestDelay = line.replacingOccurrences(of: "seconds", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: " - Request Delay: ", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "API.RequestLogCronometer.ResponseDelayQuality.normal)", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "API.RequestLogCronometer.ResponseDelayQuality.slow)", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "API.RequestLogCronometer.ResponseDelayQuality.fast)", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "s\"\", ", with: "", options: .literal, range: nil)
                    requestDelay = requestDelay.replacingOccurrences(of: "(\"\"", with: "", options: .literal, range: nil)
                    //print(requestDelay)
                    result = (requestDelay as NSString).floatValue
                }
            }
        }
        return result
    }
    var serviceResponsePretty: String {
        var result = ""
        let lines = self.components(separatedBy: "\r")
        lines.forEach { (some) in
            let isDate = some.contains(">(main) #")
            let isURL = some.contains("https://api.")
            let isTime = some.contains("Request Delay")
            if isDate || isURL || isTime {
                result = "\(result)\(some)\n "
            }
        }
        result = result.replacingOccurrences(of: "Z>(main) #", with: "")
        result = result.replacingOccurrences(of: "{✅✅✅}<", with: "Event @ ")
        result = result.replacingOccurrences(of: "{ℹ️ℹ️ℹ️}<", with: "Event @ ")
        return result
    }

    var extrateDate: String? {
        let parts = self.components(separatedBy: "\r")
        if parts.count > 0 {
            var result = parts[0]
            result = result.replacingOccurrences(of: "Z>(CaixaAPI) #", with: "")
            result = result.replacingOccurrences(of: "Z>(i9API) #", with: "")
            result = result.replacingOccurrences(of: "Z>(main) #", with: "")
            result = result.replacingOccurrences(of: "Z>(ModuleProfile) #", with: "")
            result = result.replacingOccurrences(of: "{✅✅✅}<", with: "")
            result = result.replacingOccurrences(of: "{ℹ️ℹ️ℹ️}<", with: "")
            result = result.replacingOccurrences(of: "# {⛔️⛔️⛔️}<", with: "")
            return result
        }
        return nil
    }
}

public extension Array where Element == String {
    func recordsWith(content: String) -> [String] { self.filter({ $0.contains(content) }) }
    func withDelayBigger(than: Float) -> [String] { self.filter({ $0.requestDelay ?? 0 > than }) }
    func withDelaySmaller(than: Float) -> [String] { self.filter({ $0.requestDelay ?? 1000 < than }) }
    var mediumDelay: Float {
        let requestDelays = self.map { (some) -> Float? in some.requestDelay }.filter({ $0 != nil }).filter({ $0 != nil })
        var sum: Float = 0
        requestDelays.forEach { (some) in
            if let some = some {
                sum += Float(some)
            }
        }
        return (sum / Float(requestDelays.count))
    }
    var minDelay: Float {
        let requestDelays = self.map { (some) -> Float? in some.requestDelay }.filter({ $0 != nil }).filter({ $0 != nil })
        var min: Float = 1000000
        requestDelays.forEach { (some) in
            if let some = some, min > some {
                min = some
            }
        }
        return min
    }
    var maxDelay: Float {
        let requestDelays = self.map { (some) -> Float? in some.requestDelay }.filter({ $0 != nil }).filter({ $0 != nil })
        var max: Float = 0
        requestDelays.forEach { (some) in
            if let some = some, max < some {
                max = some
            }
        }
        return max
    }
}
